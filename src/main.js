import Vue from 'vue'
import App from './App.vue'
//import VueRouter from 'vue-router'

//var VueRouter = require('vue-router')
//var About = require('./views/About.vue')
Vue.config.productionTip = false
//Vue.use(VueRouter)

/*var router = new VueRouter({
  mode: 'history',
  routes:[
    {path: '/', name: 'App'},
    {path: '/about', components: About}
  ]
})*/

new Vue({
  render: h => h(App),
}).$mount('#app')
